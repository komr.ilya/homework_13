//
//  main.cpp
//  homework_13
//
//  Created by Ilya Komrakov on 26.04.2020.
//  Copyright © 2020 Ilya Komrakov. All rights reserved.
//

#include <iostream>
#include "Helpers.h"

int main()
{
    std::cout << SquareSum(12, 3) << std::endl;
    return 0;
}
